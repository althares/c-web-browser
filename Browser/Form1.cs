﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Browser
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            go("http://www.google.com/");
        }
        private void go(string webAdress)
        {
            addres.Text = "http://" + addres.Text + "/";
            System.Object nullObject = 0;
            string str = "";
            System.Object nullObjStr = str;
            Cursor.Current = Cursors.WaitCursor;
            explorer.Navigate(webAdress);
            Cursor.Current = Cursors.Default;
        }
        private void button6_Click(object sender, EventArgs e)
        {
            go(addres.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            explorer.GoHome();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            explorer.GoBack();
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            addres.Text = Convert.ToString(explorer.Url);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            explorer.GoForward();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            explorer.Refresh();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            explorer.Stop();
        }

        private void Enter(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                go(addres.Text);

            }
            else
            {

            }
        }

        private void explorer_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            addres.Text = Convert.ToString(explorer.Url);
            Text = explorer.Document.Title;
            Text += " - Althares Sagittarius A*";
            addres.Items.Add(addres.Text);
            return;
        }

        private void addres_TextChanged(object sender, EventArgs e)
        {

        }

        private void addres_Click(object sender, EventArgs e)
        {

        }

        private void explorer_ProgressChanged(object sender, WebBrowserProgressChangedEventArgs e)
        {
            Text = explorer.Document.Title + " - Althares Sagittarius A";
            toolStripProgressBar1.Maximum = Convert.ToInt32(e.MaximumProgress);
            toolStripProgressBar1.Value = Convert.ToInt32(e.CurrentProgress);
        }

        private void Enter_Leave(object sender, KeyEventArgs e)
        {

        }

        private void addres_Enter(object sender, EventArgs e)
        {
            addres.SelectAll();
        }

    }
}
